mod packer;

use packer::{Packer, Size};

use std::io::Write;
use std::path::PathBuf;
use std::time::Instant;

use colored::Colorize;
use failure::{bail, format_err, Error};
use image::{DynamicImage, GenericImage, GenericImageView};
use structopt::StructOpt;
use walkdir::WalkDir;

impl std::str::FromStr for Size {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Error> {
        let pos = match s.find('x') {
            Some(v) => v,
            None => bail!("separator not found"),
        };

        let (a, b) = s.split_at(pos);
        Ok(Size::new(a.parse()?, b[1..].parse()?))
    }
}

#[derive(Debug)]
struct Color(u8, u8, u8, u8);

impl std::str::FromStr for Color {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Error> {
        let v = u32::from_str_radix(s, 16)?;
        Ok(Color(
            (v >> 24) as u8,
            ((v >> 16) & 0xFF) as u8,
            ((v >> 8) & 0xFF) as u8,
            (v & 0xFF) as u8,
        ))
    }
}

const ABOUT: &'static str = r#"
Sprite atlas generator.

It will recursively collect image files in the specified directory to a single atlas, and generate definition file and name-to-id map, where name is path to the sprite, without extension, relative to the input directory."#;

#[derive(StructOpt, Debug)]
#[structopt(
    name = "spritepack",
    raw(about = "ABOUT"),
    raw(setting = "structopt::clap::AppSettings::ColoredHelp")
)]
struct Opt {
    /// Generate name-to-id map
    #[structopt(short = "m", long = "map")]
    map: bool,

    /// Size of the sprite atlas (e.g. `1024x1024`)
    #[structopt(short = "s", long = "size")]
    size: Size,

    /// Output directory
    #[structopt(short = "o", long = "output", default_value = ".", parse(from_os_str))]
    output: PathBuf,

    /// Sprite atlas name
    #[structopt(short = "n", long = "name", default_value = "atlas")]
    name: String,

    /// Sprite padding (in pixels) to avoid edge bleeding
    #[structopt(short = "p", long = "padding", default_value = "1")]
    padding: u32,

    /// Directory to process
    #[structopt(name = "DIR", required = true, parse(from_os_str))]
    dir: PathBuf,

    /// Background color (RRGGBBAA)
    #[structopt(short = "b", long = "background", default_value = "00000000")]
    bg: Color,
}

fn main_inner() -> Result<(), Error> {
    let mut opt = Opt::from_args();

    opt.output = opt.output.canonicalize()?;

    let walker = WalkDir::new(&opt.dir)
        .sort_by(|a, b| a.file_name().cmp(b.file_name()))
        .into_iter()
        .filter_map(|e| e.ok());

    let mut images = Vec::new();
    let mut names = Vec::new();
    let mut rectangles = Vec::new();

    let reading_start = Instant::now();

    for entry in walker {
        if !entry.file_type().is_file() {
            continue;
        }

        let mut path = entry.path().strip_prefix(&opt.dir)?.to_owned();
        path.set_extension("");

        println!("{} {}", " Reading".green().bold(), path.display());

        let im = match image::open(entry.path()) {
            Ok(v) => v,
            Err(e) => {
                eprintln!(
                    "{} {}: {}",
                    "Skipping".yellow().bold(),
                    entry.path().display(),
                    e
                );
                continue;
            }
        };

        let (w, h) = im.dimensions();
        names.push(path.to_string_lossy().into_owned());
        rectangles.push(Size::new(w + opt.padding, h + opt.padding));
        images.push(im);
    }

    let reading_took = reading_start.elapsed();

    println!("{} {} sprites", " Packing".green().bold(), rectangles.len());
    let packing_start = Instant::now();

    let mut packer = Packer::new(opt.size);
    let packed = packer
        .pack(rectangles)
        .ok_or(format_err!("Failed to pack sprites"))?;
    let packing_took = packing_start.elapsed();

    opt.output.push(opt.name);
    opt.output.set_extension("png");

    println!(
        "{} {} ({}x{})",
        "  Saving".green().bold(),
        opt.output.display(),
        opt.size.w,
        opt.size.h
    );
    let writing_start = Instant::now();

    let mut atlas = DynamicImage::new_rgba8(opt.size.w, opt.size.h);

    for x in 0..opt.size.w {
        for y in 0..opt.size.h {
            atlas.put_pixel(
                x,
                y,
                image::Rgba {
                    data: [opt.bg.0, opt.bg.1, opt.bg.2, opt.bg.3],
                },
            );
        }
    }

    for (i, rect) in packed.iter().enumerate() {
        let image = &images[i];

        atlas.copy_from(image, rect.origin.x, rect.origin.y);
    }

    atlas.save(&opt.output)?;

    opt.output.set_extension("txt");
    println!("{} {}", "  Saving".green().bold(), opt.output.display());

    let mut f = std::fs::File::create(&opt.output)?;

    for (name, rect) in names.iter().zip(packed.iter()) {
        write!(
            f,
            "{} {} {} {} {}\n",
            name.replace("\\", "/"), rect.origin.x, rect.origin.y, rect.size.w - opt.padding, rect.size.h - opt.padding
        )?;
    }

    let writing_took = writing_start.elapsed();

    println!(
        "{} (reading {:.2?}, packing {:.2?}, writing {:.2?})",
        "Finished".green().bold(),
        reading_took,
        packing_took,
        writing_took
    );

    Ok(())
}

fn main() {
    match main_inner() {
        Err(e) => {
            eprintln!("{} {}", "error:".red().bold(), e);
            std::process::exit(1);
        }

        _ => {}
    }
}
